(ns chaikabot.irc-test
  (:require [clojure.test :refer :all]
            [chaikabot.irc :refer :all]))

(deftest extract-tokens-test
  (testing "Postfix Prefix"
    (is (= (extract-tokens ":Dummy!something@what.the.fuck.ever PRIVMSG #dev :Chaika are you ok?")
           {:from "Dummy" :to "#dev" :prefix true
            :msg "Chaika are you ok?"}))

    (is (= (extract-tokens ":Dummy!something@what.the.fuck.ever PRIVMSG #dev :are you ok, Chaika?")
           {:from "Dummy" :to "#dev" :postfix true
            :msg "are you ok, Chaika?"}))

    (is (= (extract-tokens ":Dummy!something@what.the.fuck.ever PRIVMSG #dev :Chaika?")
           {:from "Dummy" :to "#dev" :postfix true :prefix true
            :msg "Chaika?"}))

    (is (= (extract-tokens ":Dummy!something@what.the.fuck.ever PRIVMSG #dev :I sure am not talking to you")
           {:from "Dummy" :to "#dev"
            :msg "I sure am not talking to you"})))

  (testing "Bullying"
    (is (= (extract-tokens ":Dummy!something@what.the.fuck.ever PRIVMSG ChaikaBot :I sure love bullying")
           {:from "Dummy" :to "ChaikaBot" :bully true
            :msg "I sure love bullying"}))))
