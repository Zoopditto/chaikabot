(ns chaikabot.chaika-test
  (:require [clojure.test :refer :all]
            [chaikabot.chaika :refer :all]))

(deftest extract-main-clause-test
  (testing "Questions"
    (is (= (extract-main-clause "Chaika what are you doing?")
           {:content "what are you doing" :type :question}))
    (is (= (extract-main-clause "What are you doing Chaika?")
           {:content "what are you doing" :type :question}))
    (is (= (extract-main-clause "Chaika, what are you doing?")
           {:content "what are you doing" :type :question}))
    (is (= (extract-main-clause "What are you doing, Chaika?")
           {:content "what are you doing" :type :question})))

  (testing "Commands"
    (is (= (extract-main-clause "Chaika, flip a coin please.")
           {:content "flip a coin please" :type :command}))
    (is (= (extract-main-clause "Flip a coin Chaika, please.")
           {:content "flip a coin please" :type :command}))))

(deftest closed-question?-test
  (testing "Prefix"
    (is (= (closed-question? "is is real")
           true))
    (is (= (closed-question? "don't we all")
           true))
    (is (= (closed-question? "where is it")
           false)
        (= (closed-question? "whare are you doing")
           false))))
