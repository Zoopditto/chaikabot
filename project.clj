(defproject chaikabot "0.5.0"
  :description "A silly IRC bot"
  :url "http://chaika.seize.ch"
  :license {:name "Solipsistic license"
            :url "http://example.com/FIXME"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [org.clojure/core.logic "0.8.7"]
                 [org.clojure/core.typed "0.3.0-alpha2"]
                 [org.clojure/data.json "0.2.5"]
                 [compojure "1.1.8"]
                 [ring/ring-jetty-adapter "1.2.2"]
                 [ring/ring-devel "1.2.2"]
                 [ring-basic-authentication "1.0.5"]
                 [environ "0.5.0"]
                 [com.cemerick/drawbridge "0.0.6"]
                 [clj-time "0.7.0"]
                 [com.cemerick/url "0.1.1"]
                 [clj-http "1.0.1"]]
  :min-lein-version "2.0.0"
  :plugins [[environ/environ.lein "0.2.1"]
            [lein-gorilla "0.3.3"]
            [lein-typed "0.3.5"]]
  :hooks [environ.leiningen.hooks]
  :uberjar-name "chaikabot-standalone.jar"
  :profiles {:production {:env {:production true}}})
