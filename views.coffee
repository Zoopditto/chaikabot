# CouchDB views
# These are to setup in Futon before Chaikabot can access them

## /chaikabot-waifus/_design/waifu/_view/by_username
map = (doc) ->
  if doc.username? and doc.waifu?
    key = doc.username
    if doc['waifu-from']?
      value =
        'waifu': doc.waifu
        'waifu-from': doc['waifu-from']
    else
      value = waifu: doc.waifu
    emit key, value

## /chaikabot-waifus/_design/waifu/_view/by_waifu
map = (doc) ->
  if doc.username? and doc.waifu?
    key = doc.waifu
    value = doc.username
    emit key, value
