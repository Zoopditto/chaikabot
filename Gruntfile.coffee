module.exports = (grunt) ->

  grunt.initConfig {
    pkg: grunt.file.readJSON 'package.json'

    coffee:
      client:
        expand: yes
        cwd: 'views/scripts'
        src: ['*.coffee', '*/*.coffee']
        dest: 'resources/scripts'
        ext: '.js'

    jade:
      release:
        options:
          pretty: yes
          data:
            debug: no
        files:
          'resources/index.html': 'views/index.jade'

    clean:
      options:
        force: yes
      client: ['resources/*.html', 'resources/scripts/*.js']
  }

  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-jade'
  grunt.loadNpmTasks 'grunt-contrib-clean'

  grunt.registerTask 'compile', ['coffee', 'jade']
