(ns chaikabot.config)

;; Configuration files
(def conf-chaika (read-string (slurp "config/conf_chaika.edn")))
(def conf-db (read-string (slurp "config/conf_db.edn")))
(def conf-irc (read-string (slurp "config/conf_irc.edn")))
