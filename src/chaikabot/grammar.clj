(ns chaikabot.grammar
  (:require [chaikabot.config :as config]
            [clojure.string :as s]
            [clojure.core.logic :refer :all]
            [clojure.core.typed :refer [defalias ann]])
  (:import (java.util HashMap)))

(ann identifiers HashMap)
(def identifiers (:identifiers config/conf-chaika))

(defn extract-main-clause [input]
  (let [content (-> input
                    (s/lower-case)
                    (s/replace #"(chaika|\.|\,|\?)" "")
                    (s/replace #"(\s+)" " ")
                    (s/trim))
        clause-type (cond
                     (= \? (last input)) :question
                     (= "please" (last (s/split content #"\s"))) :command)]
  {:content content :type clause-type}))

(defn closed-question? [question]
  (not (empty? (run* [q]
                     (membero q (:auxiliaries identifiers))
                     (firsto (s/split question #"\s") q)))))
