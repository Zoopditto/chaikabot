(ns chaikabot.chaika
  (:require [chaikabot.config :as config]
            [chaikabot.db.waifu :as waifu]
            [chaikabot.utils :refer :all]
            [chaikabot.grammar :as g]
            [clojure.string :as s]
            [clojure.core.logic :refer :all]
            [clojure.core.typed :refer [defalias ann]]
            [clj-time.core :as t])
  (:refer-clojure :exclude [== record?]))

;; Namespace vars

(def conf config/conf-chaika)
(def lines (:lines conf))
(def identifiers (:identifiers conf))
(def usage (:usage lines))

(def answer-types
  {:chaika #""})

(declare answer-command)

(def lewd-count (atom 0))

;; Types

(defalias QueryMsg '{:msg  String
                     :from String
                     :to   String})

(defn format-line [lines & args]
  (if (sequential? lines)
    (map #(apply format (cons % args)) lines)
    (apply format (cons lines args))))

(defn get-line
  "Get a random line with the line-type keyword and format it or them using args as arguments."
  [line-type & args]
  (let [res (rand-nth (get-in lines line-type))]
    (if (sequential? res)
      (map #(apply format (cons % args)) res)
      (apply format (cons res args)))))

(defn get-magic-ans
  "Get an answers for the magic ball, using a ponderated random"
  [& args]
  (let [r (rand-int 100)
        yes-coef (get-in conf [:lines :yes :probability])
        no-coef (+ yes-coef (get-in conf [:lines :no :probability]))
        maybe-coef (+ no-coef (get-in conf [:lines :maybe :probability]))
        res (condp #(< %2 %1) r
              yes-coef :yes
              no-coef :no
              maybe-coef :maybe)]
    (get-line [res :answers] args)))

(defn answer-lewd [msg]
  (let [msg-type (rand-nth [:lewd :not-lewd])]
    (get-line [msg-type] (:from msg))))

(defn answer-bully [msg]
  (get-line [:bully] (:from msg)))

(defn answer-loli [msg]
  (get-line [:loli] (:from msg)))

(defn answer-notallowed [from]
  (get-line [:not-allowed] from))

(defn answer-roll [msg]
  (let [matches (re-find #"(\d+d\d+([\+-]\d+)? ?)+" (:msg msg))
        results (roll-dice (get matches 0))]
    (format "Roll for %s: %d (%s)"
            (:from msg)
            (compute-roll results)
            (format-roll results))))

(defn answer-get-waifu [msg]
  (let [matches (re-find #"(?i)who is (my|(\S+)'s) waifu" (:msg msg))
        name    (or (get matches 2) (:from msg))
        waifu   (waifu/get-waifu name)]
    (if waifu
        (str name "'s waifu is " (:waifu waifu))
        "Absolute mystery!")))

(defn answer-set-waifu [msg]
  (let [matches (re-find #"(?i)(my|(\S+)'s) waifu is ([\S ]+)" (:msg msg))
        name    (or (get matches 2) (:from msg))
        waifu   (last matches)
        res     (waifu/set-waifu name waifu)]
    (if res "Ok!" "Chaika, brain problems...")))

(defn greet []
  (let [now (t/now)
        h (-> now .getHourOfDay)
        d (-> now .getDayOfMonth)
        m (-> now .getMonthOfYear)
        guys (:users config/conf-irc)
        time-of-day (cond
                      (<= 0 h 4)    :greetings/night
                      (<= 5 h 11)   :greetings/morning
                      (<= 12 h 17)  :greetings/afternoon
                      (<= 18 h 23)  :greetings/evening)
        holiday     (condp = [d m]
                      [25 12] :greetings/christmas
                      [1 1]   :greetings/new-year
                      nil)]
    (get-line [(or holiday time-of-day)] guys)))

(ann answer [QueryMsg -> String])
(defmulti answer (fn []))

(defn analyse-main-clause [msg]
  (let [main-clause (g/extract-main-clause (:msg msg))]
    (if (= :question (:type main-clause))
      (get-magic-ans (:from msg))
      (answer-command (merge msg {:main-clause (:content main-clause)})))))

(defn answer-eval [msg]
  #_(try
    (let [matches (re-find #"eval (\(.*\))" (:msg msg))
          content (get matches 1)
          result (eval (read-string content))]
      (str "Result: " result))
    (catch Exception e
      (.getMessage e)))
  "Nope")

(defn analyse-msg [msg]
  (condp re-find (:msg msg)

    ;; Misc
    #"(?i)^chaika\?$"    "Yes, Chaika."
    #"(?i)^chaika\~$"    (str (:from msg) "~")
    #"(?i)^chaikabot\~$" (str (:from msg) "~")

    (re-pattern (str "(?i)" (s/join \| (:ohayou lines))))
    (format "%s %s!" (get-line [:ohayou]) (:from msg))

    (re-pattern (str "(?i)" (s/join \| (:oyasumi lines))))
    (format "%s %s"(get-line [:oyasumi]) (:from msg))
    
    #"(?i)eval \(.*\)" (answer-eval msg)

    #"(?i)roll([\S ]+)((\d+d\d+([\+-]\d+)? ?)+)" (answer-roll msg)

    #"(?i)who is (my|(\S+)'s) waifu" (answer-get-waifu msg)
    #"(?i)(my|(\S+)'s) waifu is ([\S ]+)" (answer-set-waifu msg)

    #"(?i)faggot.*?$"   (str "No, " (:from msg) " faggot.")
    #"(?i)lewd"         (answer-lewd msg)
    #"(\!|\~).*$"       (get-line [:exclamation] (:from msg))
    #"(?i)best chaika" "Chaika best Chaika"

    ;; else
    (analyse-main-clause msg)))

(defn answer-command [msg]
  (condp re-find (:msg msg)
    #"(?i)((flip|toss).*coin|coin.*(toss|flip))" (rand-nth ["Heads!" "Tails!"])
    ;;#"(?i)start.*(count|counting|keep.*(stat|tracki)).*\"\w+\"" (stat/add-counter-query msg)
    #"(?i)summon (Empi|Empireo)" (get-line [:esr] (:from msg))
    #"(?i)(ESR|Empireo Summoning ritual)" (get-line [:esr] (:from msg))
    #"(?i)fuck you" "Uwa..."

    (get-line [:confused] (:from msg))))
