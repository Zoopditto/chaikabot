(ns chaikabot.db.waifu
  (:require [chaikabot.config :refer :all]
            [chaikabot.utils :refer :all]
            [chaikabot.db.utils :as db]))

(def wdb (:waifu-db conf-db))

(defn get-waifu
  "Returns the waifu for the user"
  [username]
  (if-let [res (db/get-view wdb "waifu" "by_username" {"key" username})]
    (let [first-row  (-> res
                         (get "rows")
                         first
                         (get "value"))
          waifu      (get first-row "waifu")
          waifu-from (get first-row "waifu-from")]
      (if waifu
          {:waifu (get first-row "waifu")
           :waifu-from (get first-row "waifu-from")}
          nil))))

(defn set-waifu
  "Sets username's waifu"
  ([username waifu]
    (set-waifu username waifu nil))
  ([username waifu waifu-from]
    (if-let [res (db/get-view wdb "waifu" "by_username" {"key" username})]
      (if-let [first-row (-> res (get "rows") first)]
        ;; Updates the current document
        (let [id      (get first-row "id")
              doc     (db/get-doc wdb id)
              new-doc (merge doc {"waifu" waifu "waifu-from" waifu-from})]
          (db/put-doc wdb id new-doc))
        ;; Inserts a new document
        (db/post-doc wdb {"username" username
                          "waifu" waifu
                          "waifu-from" waifu-from})))))
