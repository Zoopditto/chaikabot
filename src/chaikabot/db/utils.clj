(ns chaikabot.db.utils
  (:require [clojure.data.json :as json]
            [clj-http.client :as http]
            [clojure.string :as s]
            [chaikabot.config :refer :all]))

(defn- fmt-url
  ([]
    (fmt-url ""))
  ([url]
    (let [{:keys [host port]} conf-db]
      (str "http://" host \: port \/ url))))

(defn- map->urlparams
  "Returns a string of url parameters from a clojure map"
  [m]
  (let [pairs (map #(str (first %)
                         \=
                         (str \" (second %) \"))
                    m)]
    (-> (apply str (interpose \& pairs))
        (s/replace #" " "+"))))

(defn- get-uuid
  "Gets fresh UUIDs from CouchDB"
  ([] (first (get-uuid 1)))
  ([count]
    (let [url (str (fmt-url) "/_uuids?count=" count)
          res (http/get url {:throw-exception false})]
      (if (= 200 (:status res))
          (-> res :body json/read-str (get "uuids"))))))

(defn post-doc
  "Inserts a document in the database db and returns its id if succeeded"
  [db doc]
  (let [url     (str (fmt-url db))
        content (json/write-str doc)
        res     (http/post url {:headers {"Content-Type" "application/json"}
                                :body content
                                :throw-exception false})]
    (if (= 201 (:status res))
        (-> res :body json/read-str (get "id")))))

(defn put-doc
  "Puts a document in the database using its id. Returns the id if succeeded"
  [db id doc]
  (let [url     (str (fmt-url db) \/ id)
        content (json/write-str doc)
        res     (http/put url {:body content :throw-exception false})]
    (if (or (= 200 (:status res))
            (= 201 (:status res)))
        (-> res :body json/read-str (get "id")))))

(defn get-doc
  "Gets a doc in the database db using its id"
  [db id]
  (let [url (str (fmt-url db) \/ id)
        res (http/get url {:throw-exception false})]
    (if (= 200 (:status res))
        (json/read-str (:body res)))))

(defn get-view
  "Gets a view from the database"
  ([db design view]
    (get-view db design view nil))
  ([db design view opt]
    (let [options (if opt (str \? (map->urlparams opt)) "")
          url     (str (fmt-url db) "/_design/" design "/_view/" view options)
          res     (http/get url {:throw-exception false})]
      (if (= 200 (:status res))
          (json/read-str (:body res))))))
