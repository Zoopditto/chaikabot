(ns chaikabot.core
  (:require [chaikabot.db :as db]
            [chaikabot.irc :as irc]
            [chaikabot.utils :as util]))

(defn -main [& args]
  ;; Scheduling a task to check on the irc thread every 10s
  ;; If dead, start a new one
  (util/schedule 500 10000 irc/watch-irc-thread))


