(ns chaikabot.utils
  (:require [clojure.java.io :as io]
            [clojure.string :as s]
            [clj-time.core :as t])
  (:import [java.util Timer TimerTask]
           (clojure.lang BigInt)))

(defn schedule
  "Uses a java.util.Timer to call fun after delay-time ms.
  And after every rep ms."
  [fun & t]
  (let [task (proxy [TimerTask] [] (run [] (fun)))
        timer (Timer.)
        [del rep] t]
    (if-not (nil? rep)
      (doto timer (.schedule task del rep))
      (doto timer (.schedule task del)))
    timer))

(defn log-error
  "Logs the message in a timestamped file in the log/ directory"
  [msg]
  (when-not (.exists (io/file "log"))
    (.mkdir (io/file "log")))
  (spit (str "log/exception-" (.toString (t/today)) ".log")
        (str (.toString (t/now)) ": " msg \return)
        :append true)
  msg)

(defn log-info
  "Logs the message in a timestamped file in the log/ directory"
  [msg server]
  (when-not (.exists (io/file "log"))
    (.mkdir (io/file "log")))
  (spit (format "log/%s-%s.log" server (.toString (t/today)))
        (str msg \return)
        :append true)
  msg)

; Dice and rolls
; ===================================

(defn roll-dice
  "Rolls dice base on a string of severall rolls, e.g. '3d20 1d10+2'
   Returns a map with the :total and the separate :rolls"
  [roll-string]
  (let [roll-parse       (fn [roll]
                           (let [matches (re-find #"(\d+)d(\d+)(([\+-])(\d+))?" roll)
                                 dice    (-> (get matches 1) Long/parseLong)
                                 value   (-> (get matches 2) Long/parseLong)
                                 mod     (symbol (or (get matches 4) "+"))
                                 mod-val (if-let [n (get matches 5)]
                                           (Long/parseLong n)
                                           0)]
                             {:dice dice :value value
                              :mod mod :mod-val mod-val
                              :modifier #((eval mod) % mod-val)}))

        roll-single-dice (fn [{:keys [value dice modifier mod mod-val]}]
                           (let [res (take dice (repeatedly #(inc (rand-int value))))]
                             {:rolls res :modifier modifier :mod mod :mod-val mod-val}))

        roll-reducer     (fn [acc elem]
                           (let [res (-> elem
                                         (roll-parse)
                                         (roll-single-dice))]
                             (conj acc res)))]

    (reduce roll-reducer
            []
            (s/split roll-string #" "))))

(defn compute-roll
  "Computes the total result of all the rolls with the modifiers applied"
  [rolls]
  (let [compute (fn [acc elem]
                  (let [mod (:modifier elem)]
                    (+' acc
                       (mod (reduce +' (:rolls elem))))))]
    (reduce compute 0 rolls)))

(defn format-roll
  "Formats the individual roll results e.g. [(12, 4, 17) +11]; [(18) -2]"
  [rolls]
  (let [mod-str (fn [roll]
                  (if-not (zero? (:mod-val roll))
                    (str (:mod roll) \space (:mod-val roll))
                    ""))
        format-single-roll (fn [roll]
                             (let [roll-res (if (> (count (:rolls roll)) 1)
                                              (->> (:rolls roll)
                                                   (reduce #(str %1 (format " %d," %2)) "")
                                                   s/trim
                                                   butlast
                                                   (apply str))
                                              (->> (:rolls roll) first str))]
                               (s/trim (format "%s %s" roll-res (mod-str roll)))))]
    (if (> (count rolls) 1)
      (->> rolls
           (reduce #(format "%s (%s)" %1 (format-single-roll %2)) "")
           s/trim)
      (format-single-roll (first rolls)))))


; IRC formatting
; ===============================

(def colors {:white       0,  :black       1
             :navy-blue   2,  :green       3
             :red         4,  :brown       5
             :purple      6,  :olive       7
             :yellow      8,  :lime-green  9
             :teal        10, :aqua-light  11
             :royal-blue  12, :hot-pink    13
             :dark-gray   14, :light-gray  15})

(defn- color
  ([fg]
   (str (char 3) (fg colors)))
  ([fg bg]
   (str (char 3) (fg colors) \, (bg colors))))

(defn c
  "Colors the text"
  ([foreground text]
   (str (color foreground)
        text
        (char 3)))
  ([foreground background text]
   (str (color foreground background)
        text
        (char 3))))

(def s
  (partial c :black :black))

(defn b
  "Boldens the text"
  [& text]
  (str (char 2) (apply str text) (char 2)))

(defn i
  "Italics the text"
  [& text]
  (str (char 9) (apply str text) (char 9)))

(defn u
  "Underlines the text"
  [& text]
  (str (char 21) (apply str text) (char 21)))

(defn r
  "Reverse the color of the text"
  [& text]
  (str (char 18) (apply str text) (char 18)))

(defn action [text]
  (str (char 1) "ACTION " text (char 1)))
