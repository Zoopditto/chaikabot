(ns chaikabot.irc
  (:import [java.net Socket SocketException]
           [java.io PrintWriter InputStreamReader BufferedReader]
           [java.lang IllegalArgumentException])
  (:require [chaikabot.chaika :as chaika]
            [chaikabot.config :as config]
            [chaikabot.utils :refer :all]
            [clojure.string :as s]))

(def conf config/conf-irc)
(def server (:server conf))
(def chan (:chan conf))
(def user (:user conf))

(def irc-status (ref {:exit true}))

(def attempts (atom 0))

(declare conn-handler)

;; Networking
;; =============================

(defn connect [server]
  (let [socket (Socket. (:name server) (:port server))
        input (BufferedReader. (InputStreamReader. (.getInputStream socket)))
        output (PrintWriter. (.getOutputStream socket))]
    (dosync (alter irc-status merge {:in input
                                     :out output
                                     :exit false}))
    (doto (Thread. #(conn-handler irc-status)) (.start))))

(defn write [conn msg]
  (doto (:out @conn)
    (.println (str msg \return))
    (.flush)))

(defn read [conn]
  (-> @conn :in .readLine (log-info (:name server))))

(defn send-msg
  "Send a message to chan. Can accept a vector of string,
  in which case it will send each message with a 1.5s interval."
  [conn chan msg]
  (when (not (nil? msg))
    (cond
      (string? msg)
      (write conn (str "PRIVMSG " chan " :" msg))
      (sequential? msg)
      (do
        (send-msg conn chan (first msg))
        (schedule #(send-msg conn chan (rest msg)) 1500)))))

(defn login [conn user]
  (write conn (str "NICK " (:nick user)))
  (write conn (str "USER " (:nick user) " 0 * :" (:name user))))

(defn join [conn chan]
  (write conn (str "JOIN " chan)))

(defn quit [conn reason]
  (dosync
    (alter conn merge {:exit true}))
  (write conn (str "QUIT " reason)))

(defn admin-quit [conn from]
  (if-not (get (:admins conf) from)
    (send-msg conn chan (chaika/answer-notallowed from))
    (quit conn "Oh noes")))

(defn ban [conn user reason]
  ())

(defn get-random-user []
  )

(defn start-ban-loop [conn]
  (schedule #(ban conn (get-random-user) ())))

(defn start-irc-thread []
  (when (<= @attempts (:attempts conf))
    (println (format "Connecting to %s on port %d..." (:name server) (:port server)))
    (try
      (connect server)
      (login irc-status user)
      (println "Login as" (:nick user))
      (join irc-status chan)
      (println "Joining" chan)
      (send-msg irc-status chan (chaika/greet))
      (start-ban-loop irc-status)
      (catch Exception e
        (println "Failed to connect: " (class e))
        (swap! attempts inc)))))

(defn watch-irc-thread []
  (when (true? (:exit @irc-status))
    (start-irc-thread)))

;; Parsing
;; ==============================

(defn- extract-msg [input tokens]
  (let [found (re-matches #"^:(\w+)\!\S+ PRIVMSG (\S+) :(.*)$" input)]
    (merge tokens {:from (found 1)
                   :to (found 2)
                   :msg (found 3)})))

(defn- detect-prefix [tokens]
  (if (re-find #"(?i)^chaika" (:msg tokens))
    (merge tokens {:prefix true})
    tokens))

(defn- detect-postfix [tokens]
  (if (re-find #"(?i)chaika.?$" (:msg tokens))
    (merge tokens {:postfix true})
    tokens))

(defn- detect-bully [tokens]
  (if (re-find #"(?i)bully" (:msg tokens))
    (merge tokens {:bully true})
    tokens))

(defn- detect-loli [tokens]
  (if (re-find #"(?i)loli" (:msg tokens))
    (merge tokens {:loli true})
    tokens))

(defn extract-tokens [input]
  (->> {}
       (extract-msg input)
       detect-prefix
       detect-postfix
       detect-bully
       detect-loli))

;; TODO: use chans

(defn conn-handler [conn]
  (while (false? (:exit @conn))
    (try
      (let [msg (read conn)]
        (condp re-find msg
          #"^ERROR :Closing Link:"
          (throw (SocketException.))
          #"^PING"
          (write conn (str "PONG " (re-find #":.*" msg)))
          #"^:(\w+)\!\S+ PRIVMSG (\S+) :(.*)$"
          (let [tokens (extract-tokens msg)]
            ;; Look for words to count
            #_(schedule #(try (stat/search-counter (:msg tokens))
                         (catch Exception _ nil)) 200)
            (cond
              ;; Usage
              (re-find #"(?i)chaika help" (:msg tokens))
              (send-msg conn (:from tokens) (chaika/usage))
              ;; Kill command
              (re-find #"(?i)chaika kill" (:msg tokens))
              (admin-quit conn (:from tokens))
              ;; Is a private message to the bot
              (= (:to tokens) (:nick user))
              (send-msg conn (:from tokens) (chaika/analyse-msg tokens))
              ;; Is addressed to the bot
              (or (true? (:prefix tokens)) (true? (:postfix tokens)))
              (send-msg conn chan (chaika/analyse-msg tokens))
              ;; Bullying prevention
              (true? (:bully tokens))
              (send-msg conn chan (chaika/answer-bully tokens))))
          ;;else
          nil))
      (catch IllegalArgumentException e
        (log-error (.toString e))
        (println "Caught exception " (type e) ": " (.getMessage e))
        (send-msg conn chan "Chaika, confused..."))
      (catch Exception e
        (log-error (.toString e))
        (println "Caught exception " (type e) ": " (.getMessage e) \return
                 "Closing IRC thread")
        (dosync (alter conn merge {:exit true}))))))
