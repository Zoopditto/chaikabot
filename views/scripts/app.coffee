angular.module 'chaika', ['ngMaterial']
.config ($mdThemingProvider) ->
  $mdThemingProvider.theme 'default'
  .primaryPalette 'brown'
  .accentPalette 'yellow'
