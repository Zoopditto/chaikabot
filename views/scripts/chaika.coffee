angular.module 'chaika'
.controller 'ChaikaCtrl', ($scope, $mdDialog, $http) ->
  @chaikaList = [
    'chaika1.jpg', 'chaika2.png', 'chaika3.png', 'chaika4.jpg',
    'chaika5.jpg', 'chaika6.jpg', 'chaika7.jpg', 'chaika8.gif'
  ]
  @currentChaika = @chaikaList[0];
  @test = "hello"

  @randChaika = =>
    @chaikaList[Math.floor(Math.random() * @chaikaList.length)]

  @changeCurrentChaika = =>
    @currentChaika = @randChaika()

  @sendMessage = (message) =>
    $http.get 'question', 
      from: 'web'
      message: message
