(function() {
  angular.module('chaika', ['ngMaterial']).config(function($mdThemingProvider) {
    return $mdThemingProvider.theme('default').primaryPalette('brown').accentPalette('yellow');
  });

}).call(this);
