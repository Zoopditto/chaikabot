(function() {
  angular.module('chaika').controller('ChaikaCtrl', function($scope, $mdDialog, $http) {
    this.chaikaList = ['chaika1.jpg', 'chaika2.png', 'chaika3.png', 'chaika4.jpg', 'chaika5.jpg', 'chaika6.jpg', 'chaika7.jpg', 'chaika8.gif'];
    this.currentChaika = this.chaikaList[0];
    this.test = "hello";
    this.randChaika = (function(_this) {
      return function() {
        return _this.chaikaList[Math.floor(Math.random() * _this.chaikaList.length)];
      };
    })(this);
    this.changeCurrentChaika = (function(_this) {
      return function() {
        return _this.currentChaika = _this.randChaika();
      };
    })(this);
    return this.sendMessage = (function(_this) {
      return function(message) {
        return $http.get('question', {
          from: 'web',
          message: message
        });
      };
    })(this);
  });

}).call(this);
